# +----------------------------------------------------------------------
# | DjangoAdmin敏捷开发框架 [ 赋能开发者，助力企业发展 ]
# +----------------------------------------------------------------------
# | 版权所有 2021~2023 北京DjangoAdmin研发中心
# +----------------------------------------------------------------------
# | Licensed LGPL-3.0 DjangoAdmin并不是自由软件，未经许可禁止去掉相关版权
# +----------------------------------------------------------------------
# | 官方网站: https://www.djangoadmin.cn
# +----------------------------------------------------------------------
# | 作者: @一米阳光 团队荣誉出品
# +----------------------------------------------------------------------
# | 版权和免责声明:
# | 本团队对该软件框架产品拥有知识产权（包括但不限于商标权、专利权、著作权、商业秘密等）
# | 均受到相关法律法规的保护，任何个人、组织和单位不得在未经本团队书面授权的情况下对所授权
# | 软件框架产品本身申请相关的知识产权，禁止用于任何违法、侵害他人合法权益等恶意的行为，禁
# | 止用于任何违反我国法律法规的一切项目研发，任何个人、组织和单位用于项目研发而产生的任何
# | 意外、疏忽、合约毁坏、诽谤、版权或知识产权侵犯及其造成的损失 (包括但不限于直接、间接、
# | 附带或衍生的损失等)，本团队不承担任何法律责任，本软件框架禁止任何单位和个人、组织用于
# | 任何违法、侵害他人合法利益等恶意的行为，如有发现违规、违法的犯罪行为，本团队将无条件配
# | 合公安机关调查取证同时保留一切以法律手段起诉的权利，本软件框架只能用于公司和个人内部的
# | 法律所允许的合法合规的软件产品研发，详细声明内容请阅读《框架免责声明》附件；
# +----------------------------------------------------------------------

from apps.models.base_db import base_db
from apps.models.base_model import base_model
from config.env import DB_PREFIX
from extends import db


# 会员模型
class Member(base_model, base_db):
    # 设置表名
    __tablename__ = DB_PREFIX + "member"
    # 用户姓名
    realname = db.Column(db.String(150), nullable=False, comment="用户姓名")
    # 用户昵称
    nickname = db.Column(db.String(150), nullable=False, comment="用户昵称")
    # 性别：1-男 2-女 3-保密
    gender = db.Column(db.Integer, default=1, comment="性别：1-男 2-女 3-保密")
    # 用户头像
    avatar = db.Column(db.String(255), nullable=False, comment="用户头像")
    # 出生日期
    birthday = db.Column(db.DateTime, nullable=False, comment="出生日期")
    # 邮箱
    email = db.Column(db.String(50), nullable=False, comment="邮箱")
    # 省份编码
    province_code = db.Column(db.String(30), nullable=False, comment="省份编码")
    # 城市编码
    city_code = db.Column(db.String(30), nullable=False, comment="城市编码")
    # 县区编码
    district_code = db.Column(db.String(30), nullable=False, comment="县区编码")
    # 省市区信息
    address_info = db.Column(db.String(255), nullable=True, comment="省市区信息")
    # 详细地址
    address = db.Column(db.String(255), nullable=False, comment="详细地址")
    # 用户名
    username = db.Column(db.String(30), nullable=True, comment="用户名")
    # 密码
    password = db.Column(db.String(255), nullable=True, comment="密码")
    # 会员等级
    member_level = db.Column(db.Integer, default=0, comment="会员等级")
    # 注册来源：1-网站注册 2-客户端注册 3-小程序注册 4-手机站注册 5-后台添加
    source = db.Column(db.Integer, default=0, comment="注册来源：1-网站注册 2-客户端注册 3-小程序注册 4-手机站注册 5-后台添加")
    # 状态：1-正常 2-禁用
    status = db.Column(db.Integer, default=1, comment="状态：1-正常 2-禁用")
    # 个人简介
    intro = db.Column(db.String(255), nullable=True, comment="个人简介")
    # 个人签名
    signature = db.Column(db.String(255), nullable=True, comment="个人签名")

    # 初始化
    def __init__(self, id, realname, nickname, gender, avatar, birthday, email, province_code, city_code, district_code,
                 address, username, password, member_level, source, status, intro, signature):
        self.id = id
        self.realname = realname
        self.nickname = nickname
        self.gender = gender
        self.avatar = avatar
        self.birthday = birthday
        self.email = email
        self.province_code = province_code
        self.city_code = city_code
        self.district_code = district_code
        self.address = address
        self.username = username
        self.password = password
        self.member_level = member_level
        self.source = source
        self.status = status
        self.intro = intro
        self.signature = signature

    def __str__(self):
        return "会员{}".format(self.id)
