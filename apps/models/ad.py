# +----------------------------------------------------------------------
# | DjangoAdmin敏捷开发框架 [ 赋能开发者，助力企业发展 ]
# +----------------------------------------------------------------------
# | 版权所有 2021~2023 北京DjangoAdmin研发中心
# +----------------------------------------------------------------------
# | Licensed LGPL-3.0 DjangoAdmin并不是自由软件，未经许可禁止去掉相关版权
# +----------------------------------------------------------------------
# | 官方网站: https://www.djangoadmin.cn
# +----------------------------------------------------------------------
# | 作者: @一米阳光 团队荣誉出品
# +----------------------------------------------------------------------
# | 版权和免责声明:
# | 本团队对该软件框架产品拥有知识产权（包括但不限于商标权、专利权、著作权、商业秘密等）
# | 均受到相关法律法规的保护，任何个人、组织和单位不得在未经本团队书面授权的情况下对所授权
# | 软件框架产品本身申请相关的知识产权，禁止用于任何违法、侵害他人合法权益等恶意的行为，禁
# | 止用于任何违反我国法律法规的一切项目研发，任何个人、组织和单位用于项目研发而产生的任何
# | 意外、疏忽、合约毁坏、诽谤、版权或知识产权侵犯及其造成的损失 (包括但不限于直接、间接、
# | 附带或衍生的损失等)，本团队不承担任何法律责任，本软件框架禁止任何单位和个人、组织用于
# | 任何违法、侵害他人合法利益等恶意的行为，如有发现违规、违法的犯罪行为，本团队将无条件配
# | 合公安机关调查取证同时保留一切以法律手段起诉的权利，本软件框架只能用于公司和个人内部的
# | 法律所允许的合法合规的软件产品研发，详细声明内容请阅读《框架免责声明》附件；
# +----------------------------------------------------------------------

from apps.models.base_db import base_db
from apps.models.base_model import base_model
from config.env import DB_PREFIX
from extends import db


# 广告模型
class Ad(base_model, base_db):
    # 设置表名
    __tablename__ = DB_PREFIX + "ad"
    # 广告标题
    title = db.Column(db.String(255), nullable=False, comment="广告标题")
    # 广告ID
    sort_id = db.Column(db.Integer, default=0, comment="广告ID")
    # 广告类型：1-图片 2-文字 3-视频 4-推荐
    type = db.Column(db.Integer, default=0, comment="广告类型：1-图片 2-文字 3-视频 4-推荐")
    # 广告封面
    cover = db.Column(db.String(255), nullable=False, comment="广告封面")
    # 广告地址
    url = db.Column(db.String(255), nullable=False, comment="广告地址")
    # 广告宽度
    width = db.Column(db.Integer, default=0, comment="广告宽度")
    # 广告高度
    height = db.Column(db.Integer, default=0, comment="广告高度")
    # 开始时间
    start_time = db.Column(db.DateTime, nullable=True, comment="开始时间")
    # 结束时间
    end_time = db.Column(db.DateTime, nullable=True, comment="结束时间")
    # 点击率
    click = db.Column(db.Integer, default=0, comment="点击率")
    # 广告状态：1-正常 2-停用
    status = db.Column(db.Integer, default=1, comment="广告状态：1-正常 2-停用")
    # 广告备注
    note = db.Column(db.String(255), nullable=True, comment="广告备注")
    # 广告排序
    sort = db.Column(db.Integer, default=0, comment="广告排序")
    # 广告内容
    content = db.Column(db.Text, nullable=True, comment="广告内容")

    # 初始化
    def __init__(self, id, title, sort_id, type, cover, url, width, height, start_time, end_time, status, note,
                 sort, content):
        self.id = id
        self.title = title
        self.sort_id = sort_id
        self.type = type
        self.cover = cover
        self.url = url
        self.width = width
        self.height = height
        self.start_time = start_time
        self.end_time = end_time
        self.status = status
        self.note = note
        self.sort = sort
        self.content = content

    def __str__(self):
        return "广告{}".format(self.title)
